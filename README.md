#moodiary

用心情记事，留住美好时光

又快又便捷的记录功能： 
1. 提供了很多好看实用的心情、天气、活动、主题标签
2. 可以同时存储文字、照片、地点、语音
3. 人性化的界面操作


您可以这样使用:
- 记录您突然的想法 
- 记录一些备忘内容
- 记录美好旅行 
- 记录一些小秘密
- 记录和朋友一起走过的美好回忆 
- 记录宝宝的成长日记 
- 记录美食照片和地点

已登录各大应用市场：
- http://zhushou.360.cn/detail/index/soft_id/29795
- http://android.myapp.com/myapp/detail.htm?apkName=com.liuzb.moodiary
- http://shouji.baidu.com/soft/item?docid=8023073

![输入图片说明](http://git.oschina.net/uploads/images/2015/1027/221219_74d7126e_65924.png "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/uploads/images/2015/1027/221237_d5759bcc_65924.png "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/uploads/images/2015/1027/221248_07091920_65924.png "在这里输入图片标题")