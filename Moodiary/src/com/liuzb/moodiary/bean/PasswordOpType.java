package com.liuzb.moodiary.bean;

public enum PasswordOpType
{
    Set, Clear, Input
}
