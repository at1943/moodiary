package com.liuzb.moodiary.listener;

public interface MoodItem
{
    void onItemClick( int position );
}
