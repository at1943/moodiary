package com.liuzb.moodiary.receiver;

import com.liuzb.moodiary.util.NotifyUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlamrReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive( Context context, Intent intent )
    {
        NotifyUtils.setNotification( context );
    }
}
