package com.liuzb.moodiary.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lidroid.xutils.db.annotation.Transient;
import com.liuzb.moodiary.bean.IconTag;
import com.liuzb.moodiary.entity.base.Entity;
import com.liuzb.moodiary.util.AttachHelper;

public class Diary extends Entity
{
    private Date date;
    private String title;
    private String content;
    private int version;

    @Transient
    private List<Attach> attaches;

    @Transient
    private ArrayList<Tag> tags;

    @Transient
    private IconTag mood;

    @Transient
    private IconTag weather;

    @Transient
    private ArrayList<IconTag> iconTags;

    public Date getDate()
    {
        return date;
    }

    public void setDate( Date date )
    {
        this.date = date;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent( String content )
    {
        this.content = content;
    }

    public int getVersion()
    {
        return version;
    }

    public void setVersion( int version )
    {
        this.version = version;
    }

    public List<Attach> getAttaches()
    {
        return attaches;
    }

    public void setAttaches( List<Attach> attaches )
    {
        this.attaches = attaches;
    }

    public ArrayList<Tag> getTags()
    {
        return tags;
    }

    public void setTags( ArrayList<Tag> tags )
    {
        this.tags = tags;
    }

    public IconTag getMood()
    {
        return mood;
    }

    public void setMood( IconTag mood )
    {
        this.mood = mood;
    }

    public IconTag getWeather()
    {
        return weather;
    }

    public void setWeather( IconTag weather )
    {
        this.weather = weather;
    }

    public ArrayList<IconTag> getIconTags()
    {
        return iconTags;
    }

    public void setIconTags( ArrayList<IconTag> iconTags )
    {
        this.iconTags = iconTags;
    }

    public String retriveTags()
    {
        return AttachHelper.retriveTag( getTags() );
    }
}
