package com.liuzb.moodiary.entity;

import com.liuzb.moodiary.entity.base.Entity;

public class DTR extends Entity
{
    private int did;
    private int tid;

    public int getDid()
    {
        return did;
    }
    public void setDid( int did )
    {
        this.did = did;
    }
    public int getTid()
    {
        return tid;
    }
    public void setTid( int tid )
    {
        this.tid = tid;
    }
}
