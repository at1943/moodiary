package com.liuzb.moodiary.entity.base;

import com.lidroid.xutils.db.annotation.Id;

public abstract class Entity
{
    @Id
    protected int id;

    public int getId()
    {
        return id;
    }

    public void setId( int id )
    {
        this.id = id;
    }
}
