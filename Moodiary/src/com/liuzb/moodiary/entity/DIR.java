package com.liuzb.moodiary.entity;

import com.liuzb.moodiary.entity.base.Entity;

public class DIR extends Entity
{
    private int did;
    private int type; // 0:mood, 1:weather, 2:tag
    private String content;

    public int getDid()
    {
        return did;
    }
    public void setDid( int did )
    {
        this.did = did;
    }
    public String getContent()
    {
        return content;
    }
    public void setContent( String content )
    {
        this.content = content;
    }
    public int getType()
    {
        return type;
    }
    public void setType( int type )
    {
        this.type = type;
    }
}
