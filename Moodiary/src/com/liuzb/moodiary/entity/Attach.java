package com.liuzb.moodiary.entity;

import android.widget.EditText;

import com.lidroid.xutils.db.annotation.Transient;
import com.liuzb.moodiary.entity.base.Entity;

/**
 * 0:photo, 1:audio, 2:gps
 */
public class Attach extends Entity
{
    private int type;
    private String name;
    private String description;
    private int did;

    @Transient
    private EditText edt;

    /**
     * 0:photo, 1:audio, 2:gps
     */
    public int getType()
    {
        return type;
    }

    /**
     * 0:photo, 1:audio, 2:gps
     * 
     * @return
     */
    public void setType( int type )
    {
        this.type = type;
    }
    public String getName()
    {
        return name;
    }
    public void setName( String name )
    {
        this.name = name;
    }
    public String getDescription()
    {
        return description;
    }
    public void setDescription( String description )
    {
        this.description = description;
    }
    public int getDid()
    {
        return did;
    }
    public void setDid( int did )
    {
        this.did = did;
    }
    public EditText getEdt()
    {
        return edt;
    }
    public void setEdt( EditText edt )
    {
        this.edt = edt;
    }
}
