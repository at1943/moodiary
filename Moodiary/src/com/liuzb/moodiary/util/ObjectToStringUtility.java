package com.liuzb.moodiary.util;

import com.liuzb.moodiary.bean.GeoBean;

public class ObjectToStringUtility
{
    public static String toString( GeoBean bean )
    {
        double[] c = bean.getCoordinates();
        return "type=" + bean.getType() + "coordinates=" + "[" + c[0] + "," + c[1] + "]";
    }
}