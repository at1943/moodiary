package com.liuzb.moodiary.util;

public class StringHelper
{
    public static boolean isNull( String string )
    {
        return ( string == null || string.equals( "" ) );
    }
}
