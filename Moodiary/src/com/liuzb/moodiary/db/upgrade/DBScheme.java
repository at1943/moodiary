package com.liuzb.moodiary.db.upgrade;

import android.database.sqlite.SQLiteDatabase;

import com.liuzb.moodiary.db.DBHelper;

public interface DBScheme
{
    void toNextVerion( DBHelper dbHelper, SQLiteDatabase db );
}
